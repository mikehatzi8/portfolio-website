import * as React from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import MCServer from '../../../assets/MCServer.PNG';

const ExpandMore = styled((props) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme, expand }) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

export default function RecipeReviewCard() {
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        <Card sx={{ maxWidth: 345 }}>
            <CardHeader
              titleTypographyProps={{variant:'h6'}}
              title="Discord Remote Server Manager"
              subheader="April, 2020"
            />

            <CardMedia
                component="img"
                height="194"
                image={MCServer}
                alt="Discord Text Log"
            />

            <CardContent>
                <Typography variant="body2" color="text.secondary">
                  At the start of the pandemic, I wanted to host a minecraft server for my friends from my laptop, but I wanted to preserve my processing power and RAM allocation
                  by only running the server when at least one person wanted it active. To automate this interaction, I created a Discord bot that would accept commands to turn on and turn off the
                  server. The Discord bot was powered by a Python 3 script that used the Windows shell to execute the server batch file.
                </Typography>
            </CardContent>

            <CardActions disableSpacing>
            
            <ExpandMore
                expand={expanded}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
            >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>
            Language: Python 3
          </Typography>
          <Typography paragraph>
            Hardware: Windows 10 laptop
          </Typography>
          <Typography paragraph>
            IDE: PyCharm
          </Typography>
          <Typography>
            Other: Discord API, Windows Shell
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}